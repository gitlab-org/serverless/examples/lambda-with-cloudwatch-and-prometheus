'use strict';

const mod = require('./badHandler');

const jestPlugin = require('serverless-jest-plugin');
const lambdaWrapper = jestPlugin.lambdaWrapper;
const wrapped = lambdaWrapper.wrap(mod, { handler: 'goodbye' });

describe('goodbye', () => {
  it('just throws an error', () => {
    wrapped.run({}).then(() => {
      throw 'Should not return successfully'
    }).catch((error) => {
      expect(error.message).toEqual('This is an error oh no!')
    })
  });
});
