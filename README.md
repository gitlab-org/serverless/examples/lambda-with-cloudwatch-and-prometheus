
# AWS Lambda and CloudWatch with Prometheus

This contains the code used for https://gitlab.com/gitlab-org/gitlab/issues/35623

---

Example project using the [Serverless Framework](https://serverless.com), JavaScript, AWS Lambda, AWS API Gateway and integrates them with AWS CloudWatch and Prometheus.

---

## Deployment

### Setting Up AWS

1. Create AWS credentials including the following IAM policies: `AWSLambdaFullAccess`, `AmazonAPIGatewayAdministrator` and `AWSCloudFormationFullAccess`.
1. Set the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` variables in the GitLab CI/CD settings. `Settings > CI/CD > Variables`.

### Configuring Metrics

1. In <project>/-/environments/metrics, you can click `Add Metric`
  ![Add Metric screenshot](docs/screenshots/add_metric.png)
1. Then, you can add the metric you would like to track. This sets up a few aws_lambda_<metrics>
  ```
  aws_lambda_errors_average
  aws_lambda_errors_maximum
  aws_lambda_errors_minimum
  aws_lambda_errors_sample_count
  aws_labmda_errors_sum
  aws_lambda_invocations_average
  aws_lambda_invocations_maximum
  aws_lambda_invocations_minimum
  aws_lambda_invocations_sample_count
  aws_labmda_invocations_sum
  ```
  ![Metric details screenshot](docs/screenshots/metric_details.png)
1. Then the new metric should appear along with your other metrics.

### Reusing this in your own cluster

We're running 3 commands in our gitlab ci deployment, [here](https://gitlab.com/gitlab-org/serverless/examples/lambda-with-cloudwatch-and-prometheus/blob/master/.gitlab-ci.yml#L19)

```
    - echo "{\"apiVersion\":\"v1\",\"data\":{\"aws.id\":\"$AWS_ACCESS_KEY_ID\",\"aws.key\":\"$AWS_SECRET_ACCESS_KEY\"},\"kind\":\"ConfigMap\",\"metadata\":{\"name\":\"aws-credentials\"}}" | kubectl apply -f -
    - kubectl apply -f exporter.kube.yml
    - kubectl annotate pod/cloudwatch-exporter-pod 'prometheus.io/scrape'=true 'prometheus.io/port'=9106
```

- The first command adds the aws credentials from your ci credentials to the cluster as a configmap
- Second, we apply the data from exporter.kube.yml, which includes the configuration we'll use for the [CloudWatch Exporter](https://github.com/prometheus/cloudwatch_exporter)
- Lastly, we annotate the pod so that prometheus will pick up the metrics automatically.

You can do this in your own project with whatever metrics you want from CloudWatch by changing the config with the metrics you want to use. I found the easiest way to look at what metric names you end up with is to port forward prometheus to your local and use the prometheus ui to autocomplete them.

## Development

### Running Locally

Install dependencies with:

```sh
npm install
```

Run backend server with:

```sh
npm start
```

This runs the serverless function locally using `serverless-offline` plugin.

Run frontend with:

```sh
npm run pages
```

The frontend should be available at `http://localhost:8080`

### Running Tests
```sh
npm test
```
